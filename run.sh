#!/usr/bin/env bash
set -e

if [[ -f secrets.sh ]]; then
  source secrets.sh
else
  echo "Please copy secrets.example.sh to secrets.sh and fill in values"
  exit 1
fi

REF_NAME=main

curl -X POST \
     --fail \
     -F token=$TOKEN \
     -F "ref=$REF_NAME" \
     -F "variables[GERRIT_CHANGE_NUMBER]=879895" \
     -F "variables[WIKILAMBDA_REF]=refs/changes/95/879895/1" \
     https://gitlab.wikimedia.org/api/v4/projects/1074/trigger/pipeline \
| jq .web_url -r